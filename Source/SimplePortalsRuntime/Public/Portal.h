#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

UCLASS()
class SIMPLEPORTALSRUNTIME_API APortal : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APortal(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintPure, Category="Portal")
	bool IsActive() const;

	UFUNCTION(BlueprintCallable, Category="Portal")
	void SetActive(bool NewActive);

	void GenerateRenderTargetTexture(uint32 SizeX, uint32 SizeY);

	// Render target to use to display the portal
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Portal")
	void ClearRenderTexture();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Portal")
	void OnRenderTargetCreated(UTexture* RenderTexture);

	static bool IsPointInFrontOfPortal(const FVector Point, const FPlane PortalPlane);

	/**
	 * @brief Converts given Transform to TargetPortal space, effectively positioning it
	 * like if it was a mirror reflection of it relative to this Portal.
	 * @param InTransform The transform to convert
	 */
	UFUNCTION(BlueprintCallable, Category="Portal")
	FTransform ConvertToTargetSpace(FTransform InTransform);

	UFUNCTION(BlueprintCallable, Category="Portal")
	void TeleportPawn(APawn* PawnToTeleport);

	UFUNCTION(BlueprintCallable, Category="Portal")
	void CaptureScene(FTransform ViewerTransform);

	UFUNCTION(BlueprintCallable, Category="Portal")
	void SetCaptureCameraFOV(float FOV);

	virtual void Tick(float DeltaSeconds) override;

protected:
	UPROPERTY(BlueprintReadOnly)
	USceneComponent* PortalRootComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneCaptureComponent2D* SceneCaptureComp;

private:
	bool bIsActive;

	UPROPERTY(EditAnywhere)
	APortal* TargetPortal;

	/**
	 * This Portal's view
	 */
	UPROPERTY(EditAnywhere)
	UTextureRenderTarget2D* RenderTarget2D;


	// Clean me up
	FVector LastPosition;
};
