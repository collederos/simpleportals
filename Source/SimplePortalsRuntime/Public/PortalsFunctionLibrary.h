#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PortalsFunctionLibrary.generated.h"

UCLASS()
class SIMPLEPORTALSRUNTIME_API UPortalsFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static FVector ConvertLocationToActorSpace(FVector Location, AActor* Reference, const AActor* Target);
	static FRotator ConvertRotationToActorSpace(FRotator Rotation, const AActor* Reference, const AActor* Target);

};
