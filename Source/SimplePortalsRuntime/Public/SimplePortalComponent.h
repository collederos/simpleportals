#pragma once

#include "CoreMinimal.h"
#include "Components/ControllerComponent.h"
#include "SimplePortalComponent.generated.h"

class APlayerController;
class APortal;
class UTextureRenderTarget2D;

UCLASS()
class SIMPLEPORTALSRUNTIME_API USimplePortalComponent : public UControllerComponent
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;
	
	// Called by a Portal Actor when wanting to teleport another Actor
	UFUNCTION(BlueprintCallable, Category="Portal")
	void RequestTeleportByPortal(APortal* Portal, APawn* TargetToTeleport);

	UFUNCTION(BlueprintCallable, Category="Portal")
	void RegisterPortal(APortal* Portal);

	UFUNCTION(BlueprintCallable, Category="Portal")
	void UnRegisterPortal(APortal* Portal);

private:
	TArray<APortal*> RegisteredPortals;
};

