#include "PortalsFunctionLibrary.h"

auto UPortalsFunctionLibrary::ConvertLocationToActorSpace(const FVector Location, AActor* Reference,
                                                          const AActor* Target) -> FVector
{
	if (Reference == nullptr || Target == nullptr)
	{
		return FVector::ZeroVector;
	}

	const FVector Direction = Location - Reference->GetActorLocation();
	const FVector TargetLocation = Target->GetActorLocation();

	FVector Dots;
	Dots.X = FVector::DotProduct(Direction, Reference->GetActorForwardVector());
	Dots.Y = FVector::DotProduct(Direction, Reference->GetActorRightVector());
	Dots.Z = FVector::DotProduct(Direction, Reference->GetActorUpVector());

	const FVector NewDirection = Dots.X * Target->GetActorForwardVector()
		+ Dots.Y * Target->GetActorRightVector()
		+ Dots.Z * Target->GetActorUpVector();

	return TargetLocation + NewDirection;
}

FRotator UPortalsFunctionLibrary::ConvertRotationToActorSpace(const FRotator Rotation, const AActor* Reference,
                                                              const AActor* Target)
{
	if (Reference == nullptr || Target == nullptr)
	{
		return FRotator::ZeroRotator;
	}

	const FTransform SourceTransform = Reference->GetActorTransform();
	const FTransform TargetTransform = Target->GetActorTransform();
	FQuat QuatRotation = FQuat(Rotation);

	const FQuat LocalQuat = SourceTransform.GetRotation().Inverse() * QuatRotation;
	const FQuat NewWorldQuat = TargetTransform.GetRotation() * LocalQuat;

	return NewWorldQuat.Rotator();
}
