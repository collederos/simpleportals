#include "Portal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
APortal::APortal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostUpdateWork;
	bIsActive = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent->Mobility = EComponentMobility::Static;

	PortalRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PortalRootComponent"));
	PortalRootComponent->SetupAttachment(GetRootComponent());
	PortalRootComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	PortalRootComponent->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	PortalRootComponent->Mobility = EComponentMobility::Movable;

	SceneCaptureComp = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("SceneCaptureComponent"));
	SceneCaptureComp->SetupAttachment(GetRootComponent());
	SceneCaptureComp->bCaptureEveryFrame = false;
	SceneCaptureComp->bCaptureOnMovement = false;
	SceneCaptureComp->bEnableClipPlane = true;
	// SceneCaptureComp->LODDistanceFactor = 3; //Force bigger LODs for faster computations
	// SceneCaptureComp->TextureTarget = nullptr;
	SceneCaptureComp->CaptureSource = SCS_FinalColorHDR;
}

void APortal::BeginPlay()
{
	Super::BeginPlay();
}


bool APortal::IsActive() const
{
	return bIsActive;
}

void APortal::SetActive(const bool NewActive)
{
	if (TargetPortal != nullptr)
	{
		bIsActive = NewActive;
	}

	if (NewActive)
		UE_LOG(LogTemp, Warning, TEXT("Trying to activate portal %s without a valid Target"), *GetName());
}

void APortal::GenerateRenderTargetTexture(uint32 SizeX = 1920, uint32 SizeY = 1080)
{
	// Use a smaller size than the current 
	// screen to reduce the performance impact
	// SizeX = FMath::Clamp(int(SizeX / 1.7), 128, 1920); //1920 / 1.5 = 1280
	// SizeY = FMath::Clamp(int(SizeY / 1.7), 128, 1080);


	// Create the RenderTarget if it does not exist
	if (RenderTarget2D == nullptr)
	{
		const FName TextureTargetFileName = MakeUniqueObjectName(this, UTextureRenderTarget2D::StaticClass(),
		                                                         FName("SimplePortalsTextureTarget"));

		// Create new RTT
		RenderTarget2D = NewObject<UTextureRenderTarget2D>(SceneCaptureComp, TextureTargetFileName);
		check(RenderTarget2D);

		RenderTarget2D->InitAutoFormat(1920, 1080);
		RenderTarget2D->RenderTargetFormat = RTF_RGBA16f;
		RenderTarget2D->Filter = TF_Bilinear;
		RenderTarget2D->ClearColor = FLinearColor::Black;
		RenderTarget2D->TargetGamma = 2.2f;

		RenderTarget2D->bNeedsTwoCopies = false;
		RenderTarget2D->AddressX = TA_Wrap;
		RenderTarget2D->AddressY = TA_Wrap;
		RenderTarget2D->bAutoGenerateMips = false;
		RenderTarget2D->bForceLinearGamma = false;

		SceneCaptureComp->TextureTarget = RenderTarget2D;
		OnRenderTargetCreated(RenderTarget2D);
	}
}

void APortal::OnRenderTargetCreated_Implementation(UTexture* RenderTexture)
{
}

bool APortal::IsPointInFrontOfPortal(const FVector Point, const FPlane PortalPlane)
{
	const float PortalDot = PortalPlane.PlaneDot(Point);

	// If < 0 means we are behind the Plane
	// See : http://api.unrealengine.com/INT/API/Runtime/Core/Math/FPlane/PlaneDot/index.html
	return (PortalDot >= 0);
}


FTransform APortal::ConvertToTargetSpace(FTransform InTransform)
{
	const FTransform RelativeToThisPortalTransform = InTransform.GetRelativeTransform(GetTransform());

	const FTransform MirroredTransform = FTransform(RelativeToThisPortalTransform.Rotator(),
	                                                RelativeToThisPortalTransform.GetLocation().
	                                                RotateAngleAxis(
		                                                180, TargetPortal->GetActorUpVector()));

	const FTransform TargetSpaceTransform = UKismetMathLibrary::ComposeTransforms(
		MirroredTransform, TargetPortal->GetTransform());

	return FTransform(
		UKismetMathLibrary::ComposeRotators(
			TargetSpaceTransform.Rotator(), FRotator(0, 180, 0)
		),
		TargetSpaceTransform.GetLocation(),
		FVector::One()
	);
}

void APortal::TeleportPawn(APawn* PawnToTeleport)
{
	if (PawnToTeleport == nullptr || TargetPortal == nullptr)
	{
		return;
	}

	const FVector OriginalVelocity = PawnToTeleport->GetMovementComponent()->Velocity;

	const FTransform TargetTransform = ConvertToTargetSpace(PawnToTeleport->GetTransform());
	PawnToTeleport->SetActorLocationAndRotation(TargetTransform.GetLocation(), TargetTransform.GetRotation());

	AController* PawnController = PawnToTeleport->GetController();
	const FRotator CurrentControlRotation = PawnController->GetControlRotation();
	const FRotator TargetControllerRotation = FRotator(CurrentControlRotation.Pitch, TargetTransform.Rotator().Yaw,
	                                                   CurrentControlRotation.Roll);
	PawnController->SetControlRotation(TargetControllerRotation);

	const FVector NewVelocity = TargetTransform.GetRotation().GetForwardVector() * OriginalVelocity.Length();
	PawnToTeleport->GetMovementComponent()->Velocity = NewVelocity;

	LastPosition = TargetTransform.GetLocation();
}

void APortal::CaptureScene(FTransform ViewerTransform)
{
	if (TargetPortal != nullptr)
	{
		const FTransform CameraToTargetTransform = ConvertToTargetSpace(ViewerTransform);
		SceneCaptureComp->SetWorldTransform(CameraToTargetTransform);
		
		SceneCaptureComp->ClipPlaneNormal = TargetPortal->GetActorForwardVector();
		SceneCaptureComp->ClipPlaneBase = TargetPortal->PortalRootComponent->GetComponentLocation()
			+ (SceneCaptureComp->ClipPlaneNormal) * -20;
		SceneCaptureComp->CaptureScene();
	}
}

void APortal::SetCaptureCameraFOV(float FOV)
{
	SceneCaptureComp->FOVAngle = FOV;
}

void APortal::Tick(float DeltaSeconds)
{
	if (IsActive())
	{
		APlayerCameraManager* PCM = UGameplayStatics::GetPlayerController(GetWorld(), 0)->PlayerCameraManager;
		CaptureScene(PCM->GetTransform());
	}
	Super::Tick(DeltaSeconds);
}

void APortal::ClearRenderTexture_Implementation()
{
}
