#include "SimplePortalComponent.h"

#include "Portal.h"


void USimplePortalComponent::BeginPlay()
{
	Super::BeginPlay();
}


void USimplePortalComponent::RequestTeleportByPortal(APortal* Portal, APawn* TargetToTeleport)
{
	if (Portal != nullptr && TargetToTeleport != nullptr)
	{	
		Portal->TeleportPawn(TargetToTeleport);
	}
}

void USimplePortalComponent::RegisterPortal(APortal* Portal)
{
	check(Portal);
	
	const APlayerController* MyController = GetController<APlayerController>();
	UGameViewportClient* Viewport = GetWorld()->GetGameViewport();
	FIntPoint ViewSize = Viewport->Viewport->GetSizeXY();
	
	Portal->GenerateRenderTargetTexture(ViewSize.X, ViewSize.Y);
	Portal->SetCaptureCameraFOV(MyController->PlayerCameraManager->GetFOVAngle());
	RegisteredPortals.AddUnique(Portal);
	
	Portal->SetActive(true);
}

void USimplePortalComponent::UnRegisterPortal(APortal* Portal)
{
	check(Portal);
	RegisteredPortals.Remove(Portal);
	Portal->SetActive(false);
}
